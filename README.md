text-game
=====

Requirements
-----

* Nodejs
* Bower (npm install -g bower)
* Grunt (npm install -g grunt-cli)

Setup
-----

Run the following commands:

    npm install
    bower install
    grunt watch & node server