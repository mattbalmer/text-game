module.exports = function(config) {
    var tasks = ['uglify', 'copy', 'stylus', 'jade'];

    function slash(path) {
        var startsWithSlash = path.indexOf(0) == '/';
        return startsWithSlash ? path : '/' + path;
    }

    function src(path) {
        return config.src + ( path ? slash(path) : '');
    }

    function dest(path) {
        return config.dest + ( path ? slash(path) : '');
    }

    function destKeys(map) {
        var result = {};

        for(var path in map) {
            if(map.hasOwnProperty(path)) {
                var key = config.dest + slash(path);
                result[key] = map[path];
            }
        }

        return result;
    }

    return function(grunt) {
        grunt.initConfig({
            pkg: grunt.file.readJSON('package.json'),
            uglify: {
                compile: {
                    options: {
                        mangle: false
                    },
                    files: destKeys({
                        'js/app.min.js': [
                            src('js/module.js'),
                            src('js/**/*.js')
                        ]
                    })
                }
            },
            stylus: {
                compile: {
                    options: {
                        paths: [ src('css/') ],
                        import: [ 'nib' ]
                    },
                    files: destKeys({
                        'css/style.css': [ src('css/**/*.styl') ]
                    })
                }
            },
            jade: {
                compile: {
                    options: {
                        pretty: true
                    },
                    files: [{
                        expand: true,
                        cwd: src('views'),
                        src: '**/*.jade',
                        dest: dest(),
                        ext: '.html'
                    }]
                }
            },
            copy: {
                images: {
                    expand: true,
                    cwd: src('images/'),
                    src: '**/*',
                    dest: dest('images/'),
                    filter: 'isFile'
                }
            },
            watch: {
                js: {
                    files: [ src('js/**/*.js') ],
                    tasks: ['uglify']
                },
                css: {
                    files: [ src('css/**/*.styl') ],
                    tasks: ['stylus']
                },
                html: {
                    files: [ src('**/*.jade') ],
                    tasks: ['jade']
                },
                images: {
                    files: [ src('images/**/*') ],
                    tasks: ['copy:images']
                }
            }
        });

        // ======================
        // === Load the stuff ===
        // ======================
        grunt.loadNpmTasks('grunt-contrib-watch');
        grunt.loadNpmTasks('grunt-contrib-uglify');
        grunt.loadNpmTasks('grunt-contrib-copy');
        grunt.loadNpmTasks('grunt-contrib-stylus');
        grunt.loadNpmTasks('grunt-contrib-jade');

        grunt.registerTask('compile', tasks);
        grunt.registerTask('default', tasks.concat(['watch']));
    };
};