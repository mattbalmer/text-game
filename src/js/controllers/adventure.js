angular.module('tg').controller('Adventure', function($scope, $routeParams, $location) {
    $scope.advId = parseInt($routeParams.advId);

    $scope.$on('actionSelected', function(event, advId) {
        console.log('a thing happened', advId);
        $scope.advId = advId;
        $location.path(advId);
    });
});