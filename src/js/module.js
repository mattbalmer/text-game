angular.module('tg', ['ngRoute'])
    .config(function($routeProvider) {
        $routeProvider
            .when('/:advId?', {
                controller: 'Adventure',
                templateUrl: 'templates/adventure.html'
            });
    });
