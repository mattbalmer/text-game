angular.module('tg').directive('adventure', function() {
    return {
        restrict: 'E',
        template: '<div ng-include="templateUrl"></div>',
        scope: {
            advId: '='
        },
        link: function($scope) {
            $scope.$watch('advId', function(event, advId) {
                $scope.templateUrl = '/adventures/' + $scope.advId + '.html';
            })
        }
    }
});