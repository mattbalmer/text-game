angular.module('tg').directive('action', function() {
    return {
        restrict: 'E',
        transclude: true,
        scope: {
            advId: '@link'
        },
        templateUrl: '/partials/action.html',
        link: function($scope) {
            $scope.click = function() {
                $scope.$emit('actionSelected', $scope.advId);
            };
        }
    }
});