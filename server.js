var path = require('path'),
    express = require('express'),
    server = express();

server.use( express.static(path.join(__dirname + '/public')) );

server.listen(3000, function() {
    console.log('listening on port 3000');
});